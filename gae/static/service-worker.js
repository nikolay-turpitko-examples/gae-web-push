self.importScripts('./dexie.min.js');

let msgsURL = new URL('/messages.html', self.location.origin).href;

function getDB() {
    let db = new Dexie('messages');
    db.version(1).stores({
        messages: '++id,read,[read+topic]'
    });
    return db;
}

async function findClients(url) {
    let c = await clients.matchAll({
            type: 'window',
            includeUncontrolled: true
        }),
        mc = [];
    for (let i = 0; i < c.length; i++) {
        const wc = c[i];
        if (wc.url === url) {
            mc.push(wc);
        }
    }
    return mc;
}

self.addEventListener('push', event => {
    const msg = event.data ? event.data.json() : {
        html: 'no payload'
    };
    msg.time = new Date();
    msg.read = 0; // 0 - false, 1 - true; bool cannot be indexed.
    event.waitUntil(async function() {
        let db = getDB(),
            mc = await findClients(msgsURL);
        await db.open();
        await db.messages.add(msg);
        let all = await db.messages.where({
                'read': 0
            }).count(),
            byTopic = await db.messages.where({
                'read': 0,
                'topic': msg.topic
            }).count(),
            visible = false;
        for (let i = 0, l = mc.length; i < l; i++) {
            mc[i].postMessage({
                msg: 'update'
            });
            if (mc[i].visibilityState === 'visible') {
                visible = true;
            }
        }
        if (visible) {
            return true;
        }
        return self.registration.showNotification(msg.title, {
            icon: 'https://cdn.icon-icons.com/icons2/872/PNG/512/anime-girl-with-long-hair_icon-icons.com_68049.png',
            tag: msg.topic,
            data: msg,
            body: 'New message in topic: ' + msg.topic +
                ', [unread by topic: ' + byTopic +
                ', unread all: ' + all + ']',
            requireInteraction: true,
            actions: [{
                action: 'open',
                title: 'Open'
            }],
        })
    }());
});

self.addEventListener('fetch', event => {
    let rq = event.request,
        url = new URL(rq.url),
        params = url.searchParams,
        action = params.get('action');
    if (url.pathname != '/messages' || rq.method !== 'GET') {
        return;
    }
    event.respondWith(async function() {
        // A bit simplified. Display and delete in fixed blocks.
        const lim = 100;
        let db = getDB();
        await db.open();
        if (!action) {
            let m = await db.messages.reverse().limit(lim).toArray();
            return new Response(JSON.stringify(m), {
                status: 200,
                statusText: 'OK'
            });
        } else if (action === 'mark-read') {
            let id = parseInt(params.get('id'), 10);
            let changed = await db.messages.update(id, { 'read': 1 });
            return new Response(null, {
                status: 202,
                statusText: 'Accepted'
            });
        } else if (action === 'clear') {
            await db.messages.reverse().limit(lim).delete();
            return new Response(null, {
                status: 202,
                statusText: 'Deleted'
            });
        } else {
            return new Response(null, {
                status: 404,
                statusText: 'Unknown action'
            });
        }
    }());
});

self.addEventListener('notificationclick', event => {
    event.waitUntil(async function() {
        event.notification.close();
        if (clients.openWindow) {
            let mc = await findClients(msgsURL);
            if (mc.length > 0) {
                return mc[0].focus();
            } else {
                return clients.openWindow(msgsURL);
            }
        }
    }());
});
