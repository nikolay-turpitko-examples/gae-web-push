async function initNotifications() {
    const serviceWorkerURL = './service-worker.js',
        publicKeyURL = './public-key',
        subscribeURL = './subscribe';
    if ('serviceWorker' in navigator) {
        try {
            const registration = await navigator.serviceWorker.register(serviceWorkerURL),
                pm = registration.pushManager;
            let subscription = await pm.getSubscription();
            if (!subscription) {
                const response = await fetch(publicKeyURL, {
                        credentials: 'same-origin'
                    }),
                    resp = await response.json(),
                    // urlBase64ToUint8Array
                    convertedVapidKey = function(base64String) {
                        const padding = '='.repeat((4 - base64String.length % 4) % 4),
                            base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/'),
                            rawData = window.atob(base64);
                        return Uint8Array.from([...rawData].map((char) => char.charCodeAt(0)));
                    }(resp.key);
                if (!response.ok) {
                    throw {
                        description: "'public key' response is not OK",
                        details: response
                    };
                }
                subscription = await pm.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: convertedVapidKey
                });
            }
            const response = await fetch(subscribeURL, {
                method: 'post',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify(subscription),
                credentials: 'same-origin'
            });
            if (!response.ok) {
                throw {
                    description: "'subscribe' response is not OK",
                    details: response
                };
            }
        } catch (e) {
            console.log(e);
        }
    } else {
        console.log('Service workers are not supported.');
    }
}

initNotifications();


document.getElementById('my-button').onclick = async function() {
    let form = new FormData(document.getElementById('my-form'));
    const response = await fetch('./notify', {
        method: 'post',
        body: form,
        credentials: 'same-origin'
    });
    if (!response.ok) {
        console.log("'notify' response is not OK", response);
    }
};
