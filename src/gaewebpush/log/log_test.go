package log

import (
	"testing"

	"github.com/stretchr/testify/require"
	"google.golang.org/appengine/aetest"
)

func TestLog(t *testing.T) {
	ctx, close, err := aetest.NewContext()
	require.NoError(t, err)
	defer close()
	Errorf(ctx, "test, %s", "xxx")
}
