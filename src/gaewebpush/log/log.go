// Package log delegates all invocations to Google App Engine log,
// but additionally log error's stack trace, so that it appear in
// the StackDriver's Error Reporting.
package log

import (
	"bytes"
	"runtime"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"
)

func Debugf(ctx context.Context, message string, args ...interface{}) {
	log.Debugf(ctx, message, args...)
}

func Infof(ctx context.Context, message string, args ...interface{}) {
	log.Infof(ctx, message, args...)
}

func Warningf(ctx context.Context, message string, args ...interface{}) {
	log.Warningf(ctx, message, args...)
}

func Errorf(ctx context.Context, message string, args ...interface{}) {
	b := make([]byte, 2048)
	// Stack trace should be formatted exactly like runtime.Stack() do it,
	// otherwise StackDrive will ignore it.
	l := runtime.Stack(b, false)
	// But it's more convenient if we remove 2 lines, corresponding to this
	// function.
	from := bytes.IndexRune(b, '\n') // retain first line
	next := from + 1
	if from >= 0 && next < l {
		to := next + bytes.IndexRune(b[next:], '\n') // first line to remove
		next = to + 1
		if to >= 0 && next < l {
			to = next + bytes.IndexRune(b[next:], '\n') // second line to remove
			if to >= 0 && to < l {
				b = append(b[:from], b[to:l]...)
			}
		}
	}
	log.Errorf(ctx, message+"\n%s", append(args, b)...)
}
