package datastore

import (
	"context"

	webpush "github.com/SherClockHolmes/webpush-go"
	"github.com/qedus/nds"
	"google.golang.org/appengine/datastore"
)

// GetWebPushKeys retrieves wep push keys from the datastore/cache, or
// generates new ones and stores them into datastore/cache.
func GetWebPushKeys(ctx context.Context) (priv, pub string, err error) {
	const keysKind = "webPushKeys"
	const keysID = 1 // single key pair is enough
	var k struct {
		Priv, Pub string
	}
	err = nds.Get(ctx, datastore.NewKey(ctx, keysKind, "", keysID, nil), &k)
	if err == nil {
		return k.Priv, k.Pub, nil
	}
	if err != datastore.ErrNoSuchEntity {
		return "", "", err
	}
	k.Priv, k.Pub, err = webpush.GenerateVAPIDKeys()
	if err != nil {
		return "", "", err
	}
	_, err = nds.Put(ctx, datastore.NewKey(ctx, keysKind, "", keysID, nil), &k)
	if err != nil {
		return "", "", err
	}
	return k.Priv, k.Pub, nil

}
