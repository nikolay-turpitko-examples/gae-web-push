package gaewebpush

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"time"

	webpush "github.com/SherClockHolmes/webpush-go"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/delay"
	"google.golang.org/appengine/user"

	"gaewebpush/bucket"
	"gaewebpush/datastore"
	"gaewebpush/log"
	"gaewebpush/push"
)

var (
	// subscriptions will lust only while server node is running
	subscriptions = map[string]webpush.Subscription{}
)

func init() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/favicon.ico", faviconHandler)
	http.HandleFunc("/public-key", publicKeyHandler)
	http.HandleFunc("/subscribe", subscribeHandler)
	http.HandleFunc("/notify", notifyHandler)

}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	err := func() error {
		u := user.Current(ctx)
		loginURL, err := user.LoginURL(ctx, "/")
		if err != nil {
			return err
		}
		logoutURL, err := user.LogoutURL(ctx, "/")
		if err != nil {
			return err
		}
		t, err := template.ParseFiles("static/index.html")
		if err != nil {
			return err
		}
		return t.Execute(w, struct {
			User      *user.User
			LoginURL  string
			LogoutURL string
		}{
			User:      u,
			LoginURL:  loginURL,
			LogoutURL: logoutURL,
		})
	}()
	if err != nil {
		log.Errorf(ctx, "%v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
}

func publicKeyHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	_, pub, err := datastore.GetWebPushKeys(ctx)
	if err != nil {
		log.Errorf(ctx, "%v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, `{"key": "%s"}`, pub)
}

func subscribeHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	u := user.Current(ctx)
	err := func() error {
		defer r.Body.Close()
		// Decode subscription
		var s webpush.Subscription
		if err := json.NewDecoder(r.Body).Decode(&s); err != nil {
			return err
		}
		subscriptions[u.ID] = s
		return nil
	}()
	if err != nil {
		log.Errorf(ctx, "%v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func notifyHandler(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	err := func() error {
		if err := r.ParseMultipartForm(2048); err != nil {
			return err
		}
		msg := r.Form.Get("msg")
		if msg == "" {
			return fmt.Errorf("got empty message")
		}
		num, _ := strconv.Atoi(r.Form.Get("num"))
		interval, _ := strconv.Atoi(r.Form.Get("interval"))
		log.Debugf(ctx, "notifyHandler params: %s, %d, %d", msg, num, interval)
		u := user.Current(ctx)
		sendNotifications.Call(ctx, *u, num, interval, msg)
		return nil
	}()
	if err != nil {
		log.Errorf(ctx, "%v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var sendNotifications = delay.Func(
	"sendNotifications",
	func(ctx context.Context, u user.User, num, interval int, msg string) {
		for i := 0; i < num; i++ {
			err := pushNotification(
				ctx,
				i%2 == 0 && !appengine.IsDevAppServer(),
				i%3 == 0,
				u,
				fmt.Sprintf("[#%d] %s", i, msg))
			if err != nil {
				log.Errorf(ctx, err.Error())
			}
			time.Sleep(time.Duration(interval) * time.Second)
		}
	})

// push.NotifyAllEx(ctx, note, isTemplate, fileName, fileType) - ?
func pushNotification(
	ctx context.Context,
	withFile, broadcast bool,
	user user.User,
	msg string) error {

	topic := "private"
	if broadcast {
		topic = "broadcast"
	}
	ttl := 2 * time.Hour
	notification := push.Notification{
		Topic: topic,
		Title: "From x-test",
		Links: []push.HyperLink{
			{
				Title: "Example link",
				URL:   "https://picsum.photos/200/300/?random",
			},
		},
	}
	if withFile {
		link, w, err := bucket.CreateFile(
			ctx,
			fmt.Sprintf(
				"sample-file-%s-%s.txt",
				user.ID,
				time.Now().Format("20060102150405")),
			"text/plain",
			ttl)
		if err != nil {
			return err
		}
		t, err := template.ParseFiles("static/file-sample-tmpl.txt")
		if err != nil {
			return err
		}
		data := struct {
			UserEmail,
			GeneratedAt,
			GeneratedBy,
			ServerSoftware,
			ReqID string
		}{
			UserEmail:      user.Email,
			GeneratedAt:    time.Now().Format(time.RFC850),
			GeneratedBy:    appengine.AppID(ctx),
			ServerSoftware: appengine.ServerSoftware(),
			ReqID:          appengine.RequestID(ctx),
		}
		err = t.Execute(w, data)
		if err != nil {
			return err
		}
		if err = w.Close(); err != nil {
			return err
		}
		notification.Links = append(
			notification.Links,
			push.HyperLink{Title: "Download file", URL: link})
	}
	var b bytes.Buffer
	t, err := template.ParseFiles("static/msg-tmpl.html")
	if err != nil {
		return err
	}
	err = t.Execute(&b, struct {
		Msg          string
		Notification push.Notification
	}{
		Msg:          msg,
		Notification: notification,
	})
	if err != nil {
		return err
	}
	notification.HTML = b.String()
	// Send Notification
	subs := []webpush.Subscription{}
	if broadcast {
		subs = append(subs, subscriptions[user.ID])
	} else {
		for _, s := range subscriptions {
			subs = append(subs, s)
		}
	}
	return push.NotifyAll(ctx, ttl, notification, subs...)
}
