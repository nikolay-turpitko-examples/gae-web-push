package bucket

import (
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/appengine"
	"google.golang.org/appengine/file"
)

// CreateFile creates file with given name and content type in the default
// bucket. Function returns url, which expires after given duration, and
// io.WriteCloser, which can be used to populate file content and must be
// closed by the caller.
func CreateFile(
	ctx context.Context,
	fileName, contentType string,
	urlExpDuration time.Duration) (url string, w io.WriteCloser, err error) {

	// Currently we are going to use text-based files (text/plain, text/csv)
	// and gzip them, if we ever need some other type, we may add rules here,
	// or extract this into argument.
	useGZip := strings.HasPrefix(contentType, "text")

	bucketName, err := file.DefaultBucketName(ctx)
	if err != nil {
		return "", nil, err
	}
	acc, err := appengine.ServiceAccount(ctx)
	if err != nil {
		return "", nil, err
	}
	url, err = storage.SignedURL(bucketName, fileName, &storage.SignedURLOptions{
		GoogleAccessID: acc,
		SignBytes: func(b []byte) ([]byte, error) {
			_, signedBytes, err := appengine.SignBytes(ctx, b)
			return signedBytes, err
		},
		Method:  "GET",
		Expires: time.Now().Add(urlExpDuration),
	})
	if err != nil {
		return "", nil, err
	}
	client, err := storage.NewClient(ctx)
	if err != nil {
		return "", nil, err
	}
	// This line is not required, according to doc, and client should be closed
	// later, along with writers. Put it into writeCloser if necessary.
	//defer client.Close()
	bucket := client.Bucket(bucketName)
	obj := bucket.Object(fileName)
	wc := obj.NewWriter(ctx)
	obj.ACL().Set(ctx, storage.AllUsers, storage.RoleReader)
	wc.ContentType = contentType
	wc.ContentDisposition = fmt.Sprintf("attachment; filename=%s", fileName)
	if useGZip {
		wc.ContentEncoding = "gzip"
		wz := gzip.NewWriter(wc)
		wz.Name = fileName
		w = &writeCloser{wz, wc}
	} else {
		w = wc
	}
	return url, w, nil
}

// writeCloser helps to close original writer after gzip one.
type writeCloser struct {
	io.WriteCloser
	c io.Closer
}

func (w *writeCloser) Close() error {
	if err := w.WriteCloser.Close(); err != nil {
		return err
	}
	return w.c.Close()
}
