package push

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	webpush "github.com/SherClockHolmes/webpush-go"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"

	"gaewebpush/datastore"
)

// HyperLink represents hyperlink within notification.
type HyperLink struct {
	Title string `json:"title"`
	URL   string `json:"url"`
}

// Notification represents push notification payload.
type Notification struct {
	// Topic or tag of the message (messages will be grouped on it in UI).
	Topic string `json:"topic"`
	// Plain-text title.
	Title string `json:"title"`
	// Formatted message, could contain HTML markup.
	HTML string `json:"html"`
	// Hyperlinks to be added at the end of the message.
	// Though hyperlinks can be embedded into the message, but
	// it may be more convenient to leave it for client to decide
	// how to handle them.
	Links []HyperLink `json:"links"`
}

// NotifyAll sends notification to all supplied subscriptions.
func NotifyAll(
	ctx context.Context,
	ttl time.Duration,
	n Notification,
	subs ...webpush.Subscription) error {

	b, err := json.Marshal(n)
	if err != nil {
		return err
	}
	priv, _, err := datastore.GetWebPushKeys(ctx)
	if err != nil {
		return err
	}
	webpushOpt := webpush.Options{
		Subscriber:      os.Getenv("APP_ADMIN_EMAIL"), // admin's email (of app server)
		Topic:           n.Topic,
		VAPIDPrivateKey: priv,
		TTL:             int(ttl / time.Second),
	}
	webpushOpt.HTTPClient = urlfetch.Client(ctx)
	ch := make(chan error, 5)
	for _, s := range subs {
		go func(s webpush.Subscription) {
			rsp, err := webpush.SendNotification(b, &s, &webpushOpt)
			if err != nil {
				ch <- err
				return
			}
			if rsp.StatusCode != http.StatusCreated {
				defer rsp.Body.Close()
				b, err := ioutil.ReadAll(rsp.Body)
				if err != nil {
					ch <- err
					return
				}
				ch <- fmt.Errorf("push server responeded with error: %s\n%s",
					rsp.Status, b)
				return
			}
			ch <- nil
		}(s)
	}
	merr := appengine.MultiError{}
	for range subs {
		err := <-ch
		if err != nil {
			merr = append(merr, err)
		}
	}
	if len(merr) > 0 {
		return merr
	}
	return nil
}
